﻿$User = $args[0]
$Word = $args[1]
$Path = $args[2]
$dateh = Get-Date -Format "yyyyMMdd-HHmm"
if ( $User -eq $null ){
    Write-Host "PLEASE WRITE A USER" 
    exit 1
}

if ( $Word -eq $null ){
    Write-Host "PLEASE WRITE A ONE OF THE 3 WORDS ( replenish | test | clean)" 
    exit 1
}

$exixts = $(try { Get-LocalUser -Name $User } catch{}) -ne $null
$direx = Test-Path C:\Users\$User\SolarSystem\

if ( $exixts -eq $false ){
    Write-Host Error - The user given not exists
    exit 1
}

if ( $Word -eq "replenish" ){
    if ( $direx -eq $true ){
        Write-Host "THE DIRECTORY WORKSPACE IT EXIXTS"
    }else{ 
        New-Item -ItemType "directory" -Path C:\Users\$User\SolarSystem\Mercury
        New-Item -ItemType "directory" -Path C:\Users\$User\SolarSystem\Venus
        New-Item -ItemType "directory" -Path C:\Users\$User\SolarSystem\Mars
        New-Item -ItemType "directory" -Path C:\Users\$User\SolarSystem\Jupiter
        New-Item -ItemType "directory" -Path C:\Users\$User\SolarSystem\Saturn
        New-Item -ItemType "directory" -Path C:\Users\$User\SolarSystem\Uranus
        New-Item -ItemType "directory" -Path C:\Users\$User\SolarSystem\Neptune
        New-Item -ItemType "File" -Path C:\Users\$User\SolarSystem\Mercury\control-planet.txt
        New-Item -ItemType "File" -Path C:\Users\$User\SolarSystem\Venus\control-planet.txt
        New-Item -ItemType "File" -Path C:\Users\$User\SolarSystem\Mars\control-planet.txt
        New-Item -ItemType "File" -Path C:\Users\$User\SolarSystem\Jupiter\control-planet.txt
        New-Item -ItemType "File" -Path C:\Users\$User\SolarSystem\Saturn\Envoriment-$User-$dateh.txt
        New-Item -ItemType "File" -Path C:\Users\$User\SolarSystem\Uranus\control-planet.txt
        New-Item -ItemType "File" -Path C:\Users\$User\SolarSystem\Neptune\control-planet.txt
         if ( $Path -eq $null ){
            exit 1
        }else{
            Write-Host "All Is Done"
    }

    }
   
}


if ( $Word -eq "test" ){
    if ( $Path -eq $null ){
        Write-Host PLEASE WRITE A DIRECTORY
        exit 1
    }
    Write-Host "Test 1 : The User $User exixts"
    Write-Host "Test 2 : The action $Word is a valid action"
    "$Path" > comprove.txt
    $solar = Get-Content comprove.txt
    if ( $solar -like '*SolarSystem*'){
        $TP = Test-Path $Path
        if ( $TP -eq $false ){
            Write-Host "The folder given to test not exixts: $Path"
            exit 1 
        }else{
            Write-Host "Test 3 : The folder given to test exixts: $Path"
            (Get-Acl $Path).Access > Permissions.txt
            $All = (Get-Content Permissions.txt | Select-String FullControl).Length
            $Read = (Get-Content Permissions.txt | Select-String Read).Length
            if ( $All -ge 1 ){
                Write-Host "$Path exixts and could be read by administrator"
            }
            if ( $Read -ge 1 ){
                Write-Host "$Path exixts and could be read by administrator"
            }
            Get-ChildItem $Path | select -Property Name > subdirectories.txt

            $LINES = (Get-Content C:\Windows\system32\subdirectories.txt).Length

            $LINESREVERSE = $LINES - 3

            $LINESTOT = $LINESREVERSE - 2

            Get-Content C:\Windows\system32\subdirectories.txt | Select -Last $LINESREVERSE > Linesreverse.txt

            Get-Content C:\Windows\system32\Linesreverse.txt | Select -First $LINESTOT > Linestot.txt

            foreach($line in [System.IO.File]::ReadLines("C:\Windows\system32\Linestot.txt"))
            {
                (Get-Acl $Path\$line).Access > Permissions.txt
                $All2 = (Get-Content Permissions.txt | Select-String FullControl).Length
                $Read2 = (Get-Content Permissions.txt | Select-String Read).Length
                if ( $All -ge 1 ){
                    Write-Host "$Path\$line exixts and could be read by administrator"
                }
                if ( $Read -ge 1 ){
                    Write-Host "$Path\$line exixts and could be read by administrator"
                }
            }

        }
    }else{
        Write-Host "the given folder has nothing to do with the solar system"
    }
}

    
if ( $Word -eq "clean" ){
    $TP = Test-Path $Path
    if ( $Path -eq $null ){
        Write-Host PLEASE WRITE A DIRECTORY
        exit 1
    }
    "$Path" > comprove.txt
    $solar = Get-Content comprove.txt
    if ( $solar -like '*SolarSystem*'){
        if ( $TP -eq $false ){
            Write-Host "THE DIRECTORY $Path NOT EXIXTS"
            exit 1
        }else{
            Remove-Item $Path -Recurse
            Write-Host "THE DIRECTORY $Path IS CLEANED"
        }
     }else{
     Write-Host "the given folder has nothing to do with the solar system"
     }
}
