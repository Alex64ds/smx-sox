root=$(ls /root)

uid1=$(id xylophone | cut -d" " -f1 | cut -d= -f2 | cut -d"(" -f1)

gid1=$(id xylophone | cut -d" " -f2 | cut -d= -f2 | cut -d"(" -f1)

uid2=$(id conductor | cut -d" " -f1 | cut -d= -f2 | cut -d"(" -f1)

gid2$=(id conductor | cut -d" " -f2 | cut -d= -f2 | cut -d"(" -f1)


if [ $root = snap ]; then

    echo "ACT 3 OF THE OPERA"

else

    exit 1

fi

orchesta=$( ls /srv | grep sox | wc -l )

if [ $orchesta -eq 0 ]; then

    mkdir /srv/sox

fi

act3=$( ls /srv/sox | grep Nocturns | wc -l )

if [ $act3 -eq 0 ]; then

    mkdir /srv/sox/Nocturns

    mkdir /srv/sox/Fratres

    mkdir /srv/sox/Adagio

    mkdir /srv/sox/DeProfundis

    echo "piccolo" > /srv/sox/Nocturns/piccolo.txt

    echo "clarinet" > /srv/sox/Nocturns/clarinet.txt

    echo "horn" > /srv/sox/Nocturns/horn.txt

    echo "trunk" > /srv/sox/Fratres/trunk.txt

    echo "fiddle" > /srv/sox/Fratres/fiddle.txt

    echo "viola" > /srv/sox/Fratres/viola.txt

    echo "cello" > /srv/sox/Adagio/cello.txt

    echo "doublebass" > /srv/sox/Adagio/doublebass.txt

    echo "battery" > /srv/sox/Adagio/battery.txt

    echo "xylophone"  > /srv/sox/DeProfundis/xylophone.txt

    echo "conductor" > /srv/sox/DeProfundis/conductor.txt

fi

chown piccolo:woodwind /srv/sox/Nocturns/piccolo.txt

chown clarinet:woodwind /srv/sox/Nocturns/clarinet.txt

chown horn:metalwind /srv/sox/Nocturns/horn.txt

chown trunk:metalwind /srv/sox/Fratres/trunk.txt

chown fiddle:strings /srv/sox/Fratres/fiddle.txt

chown viola:strings /srv/sox/Fratres/viola.txt

chown cello:strings /srv/sox/Adagio/cello.txt

chown doublebass:strings /srv/sox/Adagio/doublebass.txt

chown battery:percussion /srv/sox/Adagio/battery.txt

chown xylophone:percussion /srv/sox/DeProfundis/xylophone.txt

chown conductor:conductor /srv/sox/DeProfundis/conductor.txt

chmod 640 /srv/sox/Nocturns/piccolo.txt

chmod 640 /srv/sox/Nocturns/clarinet.txt

chmod 640 /srv/sox/Nocturns/horn.txt

chmod 660 /srv/sox/Fratres/trunk.txt

chmod 640 /srv/sox/Fratres/fiddle.txt

chmod 640 /srv/sox/Fratres/viola.txt

chmod 640 /srv/sox/Adagio/cello.txt

chmod 640 /srv/sox/Adagio/doublebass.txt

chmod 640 /srv/sox/Adagio/battery.txt

chmod 640 /srv/sox/DeProfundis/xylophone.txt

chmod 740 /srv/sox/DeProfundis/conductor.txt

touch -t 197701010000 /srv/sox/Nocturns/piccolo.txt

touch -t 197701010000 /srv/sox/Nocturns/clarinet.txt

touch -t 197701010000 /srv/sox/Nocturns/horn.txt

umask 000

ls -l /srv/sox/Nocturns

ls -l /srv/sox/Fratres

ls -l /srv/sox/Adagio

ls -l /srv/sox/DeProfundis
