#!/bin/bash

attack=$1

defense=$2

if [ $# -ne 2 ]; then

    echo "THERE IS NO BATTLE IN PROGRES"

    echo "START BATTLE : <./PokeCTR TYPE1 TYPE2>"

    exit 1

fi

type1=$(cat chart.csv | grep $attack | tail -n1 )

type2=$(cat chart.csv | grep $defense | head -n1 )

type1comp=$(cat chart.csv | grep $attack | tail -n1 | wc -l )

type2comp=$(cat chart.csv | grep $defense | head -n1 | wc -l )

typenum=$( cat chart.csv | grep -n $defense | tail -n 1 | cut -d: -f1)

if [ $type1comp -eq 0 ]; then

    echo "THE ATTACANT OR DEFENSE IS NOT IT TYPE"

    exit 1

fi

if [ $type2comp -eq 0 ]; then

    echo "THE ATTACANT OR DEFENSE IS NOT IT TYPE"

    exit 1

fi

effective=$(echo $type1 | cut -d, -f$typenum )

if [ $effective = 1 ]; then

    echo "IT'S NEUTRAL"

fi

if [ $effective = 2 ]; then

    echo "IT'S SUPER EFECTIVE"

fi

if [ $effective = 0.5 ]; then

    echo "IT'S VERY NOT EFECTIVE"

fi

if [ $effective = 0 ]; then

    echo "IT'S INMUNE"

fi
