﻿COMO CONFIGURAR PHPLDAP Instalamos el Sldap

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.001.png)

Nos pedirá poner una contraseña la cual pondremos

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.002.png)

Instalamos el phpLPADadmin

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.003.png)

Reconfiguraremos con el comando dpkg-reconfigure slapd

Omitiremos la configuración del openLDAP

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.004.png)

Le ponemos el nombre de nuestro dominio DNS

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.005.png)

y también a nuestra organización

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.006.png)

y obviamente hay que poner una contraseña

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.007.png)

Le decimos que no queremos que se borre la base de datos despues de purgar el slapd

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.008.png)

Y movemos la base antigua

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.009.png)

Bajamos el .zip del ghithub

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.010.png)

Lo descomprimimos con el comando unzip 1.2.6.4.zip

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.011.png)

Nos cargamos el contenido del directorio del phpldapadmin sin borrar dicho directorio

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.012.png)

Copiamos el contenido del ziop descomprimido al directorio del phpldapadmin

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.013.png)

el archivo del confic.php.example le cambiamos el nombre a config.php

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.014.png)

Iniciamos sesion (la pagina es http://IP/phpldapadmin/ )

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.015.jpeg)Y creamos todos los usuarios y grupos que hagan falta (para hacerlo hay que clicar en o crear nuevo objeto o en crear un objeto hijo después de haber clicado a uno de los usuarios)

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.016.jpeg)

Hay que seleccionar la plantilla más adecuada para cada usuarios

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.017.jpeg)

Para ver los usuarios del LDAP desde la terminal instalamos el ldap-utils en un cliente u otro server

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.018.jpeg)

y ponemos ldapsearch -x -b con su dominio y la ip del servidor

![](Aspose.Words.a254736d-c262-484c-a496-4453c4b4dd5e.019.jpeg)
