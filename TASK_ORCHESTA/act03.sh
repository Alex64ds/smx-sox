#!/bin/bash

root=$(ls /root)

if [ $root = snap ]; then

    echo "ACT 3 OF THE OPERA"

else

    exit 1

fi

user1=$( cat /etc/passwd | cut -d: -f1 | grep piccolo | wc -l)

user2=$( cat /etc/passwd | cut -d: -f1 | grep clarinet | wc -l)

user3=$( cat /etc/passwd | cut -d: -f1 | grep horn | wc -l)

user4=$( cat /etc/passwd | cut -d: -f1 | grep trunk | wc -l)

user5=$( cat /etc/passwd | cut -d: -f1 | grep fiddle | wc -l)

user6=$( cat /etc/passwd | cut -d: -f1 | grep viola | wc -l)

user7=$( cat /etc/passwd | cut -d: -f1 | grep cello | wc -l)

user8=$( cat /etc/passwd | cut -d: -f1 | grep doublebass | wc -l)

user9=$( cat /etc/passwd | cut -d: -f1 | grep battery | wc -l)

user10=$( cat /etc/passwd | cut -d: -f1 | grep xylophone | wc -l)

user11=$( cat /etc/passwd | cut -d: -f1 | grep conductor | wc -l)

#COMPROVE GROUP EXIXTS

group1=$(cat /etc/group | cut -d: -f1 | grep strings | wc -l)

group2=$(cat /etc/group | cut -d: -f1 | grep woodwind | wc -l)

group3=$(cat /etc/group | cut -d: -f1 | grep metalwind | wc -l)

group4=$(cat /etc/group | cut -d: -f1 | grep percussion | wc -l)

group5=$(cat /etc/group | cut -d: -f1 | grep conductor | wc -l)

group6=$(cat /etc/group | cut -d: -f1 | grep orchesta | wc -l)

#CREATE GROUPS THET NOT EXIXT

if [ $group1 -eq 0 ]; then

    groupadd strings

    echo "GROUP STRINGS CREATED"

else

    echo "GROUP STRINGS IT EXIXTS"

fi

if [ $group2 -eq 0 ]; then

    groupadd woodwind

    echo "GROUP WOODWIND CREATED"

else

    echo "GROUP WOODWIND IT EXIXTS"

fi

if [ $group3 -eq 0 ]; then

    groupadd metalwind

    echo "GROUP METALWIND CREATED"

else

    echo "GROUP METALWIND IT EXIXTS"

fi

if [ $group4 -eq 0 ]; then

    groupadd percussion

    echo "GROUP PERCUSSION CREATED"

else

    echo "GROUP PERCUSSION IT EXIXTS"

fi

if [ $group5 -eq 0 ]; then

    groupadd conductor

    echo "GROUP CONDUCTOR CREATED"

else

    echo "GROUP CONDUCTOR IT EXIXTS"

fi

if [ $group6 -eq 0 ]; then

    groupadd orchesta

    echo "GROUP ORCHESTA CREATED"

else

    echo "GROUP ORCHESTA IT EXIXTS"

fi

sleep 1

#CREATING USERS NOT EXISTING AND ADDING-ING IN THE GROUPS CORRECTLY

if [ $user1 -eq 0 ]; then

    useradd piccolo -m -g orchesta -G woodwind -p 1234

    echo "THE PICCOLO IS BUYED"

else

    echo "THE PICCOLO IT EXIXTS"

fi

if [ $user2 -eq 0 ]; then

    useradd clarinet -m -g orchesta -G woodwind -p 1234

    echo "THE CLARINET IS BUYED"

else

    echo "THE CLARINET IT EXIXTS"

fi

if [ $user3 -eq 0 ]; then

    useradd horn -m -g orchesta -G metalwind -p 1234

    echo "THE HORN IS BUYED"

else

    echo "THE HORN IT EXIXTS"

fi

if [ $user4 -eq 0 ]; then

    useradd trunk -m -g orchesta -G metalwind -p 1234

    echo "THE TRUNK IS BUYED"

else

    echo "THE TRUNK IT EXIXTS"

fi

if [ $user5 -eq 0 ]; then

    useradd fiddle -m -g orchesta -G strings -p 1234

    echo "THE FIDDLE IS BUYED"

else

    echo "THE FIDDLE IT EXIXTS"

fi

if [ $user6 -eq 0 ]; then

    useradd viola -m -g orchesta -G strings -p 1234

    echo "THE VIOLA IS BUYED"

else

    echo "THE VIOLA IT EXIXTS"

fi

if [ $user7 -eq 0 ]; then

    useradd cello -m -g orchesta -G strings -p 1234

    echo "THE CELLO IS BUYED"

else

    echo "THE CELLO IT EXIXTS"

fi

if [ $user8 -eq 0 ]; then

    useradd doublebass -m -g orchesta -G strings -p 1234

    echo "THE DOUBLEBASS IS BUYED"

else

    echo "THE DOUBLEBASS IT EXIXTS"

fi

if [ $user9 -eq 0 ]; then

    useradd battery -m -g orchesta -G percussion -p 1234

    echo "THE BATTERY IS BUYED"

else

    echo "THE BATTERY IT EXIXTS"

fi

if [ $user10 -eq 0 ]; then

    useradd xylophone -m -g orchesta -G percussion -p 1234

    echo "THE XYLOPHONE IS BUYED"

else

    echo "THE XYLOPHONE IT EXIXTS"

fi

if [ $user11 -eq 0 ]; then

    useradd conductor -m -g orchesta -G conductor -p 1234

    echo "THE CONDUCTOR IS CONTRATED"

else

    echo "THE CONDUCTOR IT EXIXTS"

fi

uid1=$(id xylophone | cut -d" " -f1 | cut -d"=" -f2 | cut -d"(" -f1 )

gid1=$(id xylophone | cut -d" " -f2 | cut -d"=" -f2 | cut -d"(" -f1 )

uid2=$(id conductor | cut -d" " -f1 | cut -d"=" -f2 | cut -d"(" -f1 )

gid2=$(id conductor | cut -d" " -f2 | cut -d"=" -f2 | cut -d"(" -f1 )

orchesta=$( ls /srv | grep sox | wc -l )

if [ $orchesta -eq 0 ]; then

    mkdir /srv/sox

fi

act3=$( ls /srv/sox | grep Nocturns | wc -l )

if [ $act3 -eq 0 ]; then

    mkdir /srv/sox/Nocturns

    mkdir /srv/sox/Fratres

    mkdir /srv/sox/Adagio

    mkdir /srv/sox/DeProfundis

    mkdir /srv/sox/Adagio/secrets_of_director

    echo "piccolo" > /srv/sox/Nocturns/piccolo.txt

    echo "clarinet" > /srv/sox/Nocturns/clarinet.txt

    echo "horn" > /srv/sox/Nocturns/horn.txt

    echo "trunk" > /srv/sox/Fratres/trunk.txt

    echo "fiddle" > /srv/sox/Fratres/fiddle.txt

    echo "viola" > /srv/sox/Fratres/viola.txt

    echo "cello" > /srv/sox/Adagio/cello.txt

    echo "doublebass" > /srv/sox/Adagio/doublebass.txt

    echo "battery" > /srv/sox/Adagio/battery.txt

    echo "xylophone $uid1 $gid1"  > /srv/sox/DeProfundis/xylophone.txt

    echo "conductor $uid2 gid2" > /srv/sox/DeProfundis/conductor.txt

fi

chgrp orchesta /srv/sox/Fratres

chown piccolo:woodwind /srv/sox/Nocturns/piccolo.txt

chown clarinet:woodwind /srv/sox/Nocturns/clarinet.txt

chown horn:metalwind /srv/sox/Nocturns/horn.txt

chown trunk:metalwind /srv/sox/Fratres/trunk.txt

chown fiddle:strings /srv/sox/Fratres/fiddle.txt

chown viola:strings /srv/sox/Fratres/viola.txt

chown cello:strings /srv/sox/Adagio/cello.txt

chown doublebass:strings /srv/sox/Adagio/doublebass.txt

chown battery:percussion /srv/sox/Adagio/battery.txt

chown conductor:orchesta /srv/sox/Adagio/secrets_of_director

chown xylophone:percussion /srv/sox/DeProfundis/xylophone.txt

chown conductor:conductor /srv/sox/DeProfundis/conductor.txt

chmod 640 /srv/sox/Nocturns/piccolo.txt

chmod 640 /srv/sox/Nocturns/clarinet.txt

chmod 640 /srv/sox/Nocturns/horn.txt

chmod 660 /srv/sox/Fratres/trunk.txt

chmod 640 /srv/sox/Fratres/fiddle.txt

chmod 640 /srv/sox/Fratres/viola.txt

chmod g+s /srv/sox/Fratres

chmod 640 /srv/sox/Adagio/cello.txt

chmod 640 /srv/sox/Adagio/doublebass.txt

chmod 640 /srv/sox/Adagio/battery.txt

chmod 740 /srv/sox/Adagio/secrets_of_director

chmod 640 /srv/sox/DeProfundis/xylophone.txt

chmod 740 /srv/sox/DeProfundis/conductor.txt

touch -t 197701010000 /srv/sox/Nocturns/piccolo.txt

touch -t 197701010000 /srv/sox/Nocturns/clarinet.txt

touch -t 197701010000 /srv/sox/Nocturns/horn.txt

ls -l /srv/sox/

ls -l /srv/sox/Nocturns

ls -l /srv/sox/Fratres

ls -l /srv/sox/Adagio

ls -l /srv/sox/DeProfundis
