#!/bin/bash

h=$( history )

root=$(ls /root)

if [ $root = snap ]; then

    echo "ACT 4 OF THE OPERA"

else

    exit 1

fi

orchesta=$( ls /srv | grep sox | wc -l )

if [ $orchesta -eq 0 ]; then

    mkdir /srv/sox

fi

act2=$( ls /srv/sox | grep Gallop | wc -l )

if [ $act2 -eq 0 ]; then

    mkdir /srv/sox/Gallop

    mkdir /srv/sox/Carmina

    mkdir /srv/sox/1812

    echo "piccolo" > /srv/sox/Gallop/piccolo.txt

    echo "clarinet | $h" > /srv/sox/1812/clarinet.txt

    echo "horn" > /srv/sox/Gallop/horn.txt

    echo "trunk" > /srv/sox/Carmina/trunk.txt

    echo "fiddle" > /srv/sox/Carmina/fiddle.txt

    echo "viola | violin | harp | fiddle | cello | doublebass" > /srv/sox/Gallop/viola.txt

    echo "cello" > /srv/sox/Carmina/cello.txt

    echo "doublebass" > /srv/sox/Carmina/doublebass.txt

    echo "battery | $h" > /srv/sox/1812/battery.txt

    echo "xylophone | $h" > /srv/sox/1812/xylophone.txt

    cat /srv/sox/Gallop/piccolo.txt /srv/sox/1812/clarinet.txt /srv/sox/Gallop/horn.txt /srv/sox/Carmina/trunk.txt /srv/sox/Carmina/fiddle.txt /srv/sox/Gallop/viola.txt /srv/sox/Carmina/cello.txt /srv/sox/Carmina/doublebass.txt /srv/sox/1812/battery.txt /srv/sox/1812/xylophone.txt > /srv/sox/Carmina/conductor.txt

fi

chown piccolo:woodwind /srv/sox/Gallop/piccolo.txt

chown clarinet:woodwind /srv/sox/1812/clarinet.txt

chown horn:metalwind /srv/sox/Gallop/horn.txt

chown trunk:metalwind /srv/sox/Carmina/trunk.txt

chown fiddle:strings /srv/sox/Carmina/fiddle.txt

chown viola:strings /srv/sox/Gallop/viola.txt

chown cello:strings /srv/sox/Carmina/cello.txt

chown doublebass:strings /srv/sox/Carmina/doublebass.txt

chown battery:percussion /srv/sox/1812/battery.txt

chown xylophone:percussion /srv/sox/1812/xylophone.txt

chown conductor:conductor /srv/sox/Carmina/conductor.txt

chmod 640 /srv/sox/Gallop/piccolo.txt

chmod 640 /srv/sox/1812/clarinet.txt

chmod 640 /srv/sox/Gallop/horn.txt

chmod 660 /srv/sox/Carmina/trunk.txt

chmod 640 /srv/sox/Carmina/fiddle.txt

chmod 640 /srv/sox/Gallop/viola.txt

chmod 640 /srv/sox/Carmina/cello.txt

chmod 640 /srv/sox/Carmina/doublebass.txt

chmod 640 /srv/sox/1812/battery.txt

chmod 640 /srv/sox/1812/xylophone.txt

chmod 640 /srv/sox/Carmina/conductor.txt

ls -l /srv/sox/Gallop

ls -l /srv/sox/Saturn

ls -l /srv/sox/1812
