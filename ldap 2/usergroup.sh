#!/bin/bash

rt=$(sudo ls /root | wc -l )

us=$( whoami )

ipser=$( dig ubuntusrv.smx2023.net | tail -n 7 | head -n 1 | cut -f5 )

grp=$(ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H ldap://ubuntusrv.smx2023.net | grep $us | sed '2!d' | cut -d "," -f2 | cut -d= -f2)

compmount=$(mount | grep "$HOME/shared/smx2023.net/troops" | wc -l)

if [ $compmount -ne 0 ]; then

    echo "EL DIRECTORIO $HOME/shared/smx2023.net/troops YA ESTA MONTADO POR TANTO NO HACE FALTA VOLVER A MONTARLO"

    exit 1

fi


if [ $rt -eq 0 ]; then

    echo "EL USUARIO $us NO TIENE PERMISO DE ROOT POR TANTO NO PODRÁ MONTAR NADA"

    logger "permissions=no"

    exit 1
fi

if [ -d $HOME/shared ]; then

    echo " "

else

    mkdir $HOME/shared

fi

if [ -d $HOME/shared/smx2023.net ]; then

    echo " "

else

    mkdir $HOME/shared/smx2023.net

fi

if [ -d $HOME/shared/smx2023.net/troops ]; then

    echo " "

else

    mkdir $HOME/shared/smx2023.net/troops

    logger "Clidirects created"

fi

if [ $grp = goblins ]; then

    logger "goblin=si"

    sudo mount $ipser:/srv/troops/ $HOME/shared/smx2023.net/troops

    mnt=$(mount | grep "$HOME/shared/smx2023.net/troops")

    if [ -n "$mnt" ]; then

        logger "montsi"

        echo "el directorio se montó correctamente"

    else

        logger "montno"

        echo "el directorio no fue montado correctamente"

    fi

else

    logger "goblin=no"

    echo "EL USUARIO CON EL QUE ESTAS LOGADO NO PERTENECE AL GRUPO DE LOS GOBLINS"

fi

exit 0


