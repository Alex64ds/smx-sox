﻿INSTALACIÓN DEL CLIENTE LDAP

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.001.png)

Le decimos al cliente que LDAP server se usará

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.002.png)

Le damos el nombre base del servidor

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.003.png)

Le decimos la version 3

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.004.png)

Le decimos que si queremos crear un root database admin

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.005.png)

Le decimos que no queremos Database login

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.006.png)

Le decimos que cuanta LDAP tendrá root

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.007.png)

Y obviamente su contraseña

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.008.png)

LDAP & TLS

Instalamos los paquetes gnutls-bin y ssl-cert en el servidor LDAP

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.009.png)

Generamos la clave privada del certificado de autoridad

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.010.png)

Creamos el fichero /etc/ssl/ca.info para definir el CA

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.011.png)

Creamos el CA firmado

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.012.jpeg)

Añadimos el certificao

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.013.png)

Generamos la clave privada

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.014.png)

Creamos el fichero /etc/ssl/ldap01.info en el que le meteremos

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.015.png)

Creamos el certificado del servidor

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.016.jpeg)

Ajuntamos permisos y propietarios

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.017.png)

Creamos el fichero certinfo.ldif en el que le meteremos esto

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.018.png)

Usamos el ldapmodify para que el LDAP sepa los cambios hechos

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.019.png)

Vamos al fichero /etc/default/sldap en el que en la linea que tiene SLDAP\_SERVICES añadimos el ldaps

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.020.png)

Reiniciamos servicio con sudo service slapd restart

Y testeamos LDAPS (recuerda primero poner el dominio en /etc/hosts)

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.021.png)

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.022.png)

INSTALACIÓN SSSD

Los paquetes a instalar son libpam-sss y libnss-sss

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.023.jpeg)

Creamos el fichero /etc/sssd/sssd.conf y le meteremos esto

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.024.jpeg)

Servidor OpenLDAP como CA en el Cliente Descargamos el certificado con este comando

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.025.jpeg)

Se recomienda poner el dominio en el /etc/hosts

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.026.png)

Intentamos validar el certificado pero por defecto nos dara error

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.027.png)

Para solucionarlo meteremos el certificado en /etc/ssl/certs/ldapcacert.crt

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.028.png)

Y ya no saldrá ningun tipo de error

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.029.jpeg)

Ahora cambiaremos en el fichero /etc/ldap/ldap.conf el directorio del TLS\_CACERT

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.030.png)

Cambiamos dueño, grupo y permisos de los ficheros de /etc/sssd

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.031.png)

Reiniciamos el servicio sssd

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.032.png)

Si nos da error le hacemos un status al sssd

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.033.jpeg)

Y instalaremos el paquete libsss-simpleifp0

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.034.png)

Y despues podremos comprobar que nos reinicia correctamente

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.035.jpeg)

PAM MKHOMEDIR

Debajo de session optional pam\_sss.so ponemos session required pam\_mkhomedir.so skel=/etc/skel/ umask=0022

![](Aspose.Words.65be8cc3-5d58-40ba-bd9f-4c4fbb594099.036.png)

Y así podrémos hacer el getent passwd y nos saldrá el usuario

![](pasgo.png)

Y podremos hasta logarnos con el

![](logldap.png)