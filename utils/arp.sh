#!/bin/bash

var=0

rc=0

while [ $var -eq 0 ]; do

    wget http://tic.ieslasenia.org/arp.log

    cat arp.log | cut -d" " -f5 | sort | uniq > pass-listos.txt

    cat pass-listos.txt | while read line
    do

       macex=$( echo "$line" | wc -L )

        if [ $macex -eq 17 ]; then

            ip=$(cat arp.log | grep "$line" | cut -d" " -f1 | uniq -d | wc -l)

            mac=$(cat arp.log | grep "$line" | cut -d" " -f5 | uniq -d | wc -l)

        fi

        if [ $ip -eq $mac ]; then

            let rc=rc+1

        else

            echo "LA MAC $line ESTA DUPLICADA"

        fi

    done

    sleep 1

    rm -r arp.log

done
