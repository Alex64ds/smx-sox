#!/bin/bash

root=$(ls /root)

if [ $root = snap ]; then

    echo "ACT 2 OF THE OPERA"

else

    exit 1

fi

orchesta=$( ls /srv | grep sox | wc -l )

if [ $orchesta -eq 0 ]; then

    mkdir /srv/sox

fi

act2=$( ls /srv/sox | grep Sinfonietta | wc -l )

if [ $act2 -eq 0 ]; then

    mkdir /srv/sox/Sinfonietta

    mkdir /srv/sox/Saturn

    mkdir /srv/sox/Unfished

    mkdir /srv/sox/Valkyries

    echo "piccolo" > /srv/sox/Sinfonietta/piccolo.txt

    echo "clarinet" > /srv/sox/Sinfonietta/clarinet.txt

    echo "horn" > /srv/sox/Sinfonietta/horn.txt

    echo "SILENCE" > /srv/sox/Sinfonietta/TheSilencio.txt

    echo "trunk" > /srv/sox/Saturn/trunk.txt

    echo "fiddle" > /srv/sox/Saturn/fiddle.txt

    echo "viola" > /srv/sox/Unfished/viola.txt

    echo "cello" > /srv/sox/Unfished/cello.txt

    echo "doublebass" > /srv/sox/Saturn/doublebass.txt

    echo "battery" > /srv/sox/Unfished/battery.txt

    echo "The swift Indian bat happily ate cardillo and kiwi, while the stork played the saxophone behind the straw hut .... 0123456789" > /srv/sox/Valkyries/xylophone.txt

    echo "The swift Indian bat happily ate cardillo and kiwi, while the stork played the saxophone behind thestraw hut .... 0123456789" > /srv/sox/Valkyries/conductor.txt

fi

chown piccolo:woodwind /srv/sox/Sinfonietta/piccolo.txt

chown clarinet:woodwind /srv/sox/Sinfonietta/clarinet.txt

chown horn:metalwind /srv/sox/Sinfonietta/horn.txt

chown conductor:orchesta /srv/sox/Sinfonietta/TheSilencio.txt

chown trunk:metalwind /srv/sox/Saturn/trunk.txt

chown fiddle:strings /srv/sox/Saturn/fiddle.txt

chown viola:strings /srv/sox/Unfished/viola.txt

chown cello:strings /srv/sox/Unfished/cello.txt

chown doublebass:strings /srv/sox/Saturn/doublebass.txt

chown battery:percussion /srv/sox/Unfished/battery.txt

chown xylophone:percussion /srv/sox/Valkyries/xylophone.txt

chown conductor:conductor /srv/sox/Valkyries/conductor.txt

chmod 640 /srv/sox/Sinfonietta/piccolo.txt

chmod 640 /srv/sox/Sinfonietta/clarinet.txt

chmod 640 /srv/sox/Sinfonietta/horn.txt

chmod 744 /srv/sox/Sinfonietta/TheSilencio.txt

chmod 660 /srv/sox/Saturn/trunk.txt

chmod 640 /srv/sox/Saturn/fiddle.txt

chmod 641 /srv/sox/Unfished/viola.txt

chmod 640 /srv/sox/Unfished/cello.txt

chmod 640 /srv/sox/Saturn/doublebass.txt

chmod 640 /srv/sox/Unfished/battery.txt

chmod 640 /srv/sox/Valkyries/xylophone.txt

chmod 740 /srv/sox/Valkyries/conductor.txt

ls -l /srv/sox/Sinfonieta

ls -l /srv/sox/Saturn

ls -l /srv/sox/Unfished

ls -l /srv/sox/Valkyries
