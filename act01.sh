#!/bin/bash

root=$(ls /root)

if [ $root = snap ]; then

    echo "ACT 1 OF THE OPERA"

else

    exit 1

fi

orchesta=$( ls /srv | grep sox | wc -l )

if [ $orchesta -eq 0 ]; then

    mkdir /srv/sox

fi

act1=$( ls /srv/sox | grep TheGreatGateOfKiev | wc -l )

if [ $act1 -eq 0 ]; then

    mkdir /srv/sox/TheGreatGateOfKiev

    mkdir /srv/sox/BlueDanube

    mkdir /srv/sox/NewWorldSymphony

    mkdir /srv/sox/TheJazzSuite

    echo "piccolo" > /srv/sox/TheGreatGateOfKiev/piccolo.txt

    echo "clarinet" > /srv/sox/TheGreatGateOfKiev/clarinet.txt

    echo "horn" > /srv/sox/TheGreatGateOfKiev/horn.txt

    echo "trunk" > /srv/sox/BlueDanube/trunk.txt

    echo "fiddle" > /srv/sox/BlueDanube/fiddle.txt

    echo "viola" > /srv/sox/BlueDanube/viola.txt

    echo "cello" > /srv/sox/NewWorldSymphony/cello.txt

    echo "doublebass" > /srv/sox/NewWorldSymphony/doublebass.txt

    echo "battery" > /srv/sox/TheJazzSuite/battery.txt

    echo "xylophone" > /srv/sox/TheJazzSuite/xylophone.txt

    echo "conductor" > /srv/sox/TheJazzSuite/conductor.txt

fi

chown piccolo:woodwind /srv/sox/TheGreatGateOfKiev/piccolo.txt

chown clarinet:woodwind /srv/sox/TheGreatGateOfKiev/clarinet.txt

chown horn:metalwind /srv/sox/TheGreatGateOfKiev/horn.txt

chown trunk:metalwind /srv/sox/BlueDanube/trunk.txt

chown fiddle:strings /srv/sox/BlueDanube/fiddle.txt

chown viola:strings /srv/sox/BlueDanube/viola.txt

chown cello:strings /srv/sox/NewWorldSymphony/cello.txt

chown doublebass:strings /srv/sox/NewWorldSymphony/doublebass.txt

chown battery:percussion /srv/sox/TheJazzSuite/battery.txt

chown xylophone:percussion /srv/sox/TheJazzSuite/xylophone.txt

chown conductor:conductor /srv/sox/TheJazzSuite/conductor.txt

chmod 640 /srv/sox/TheGreatGateOfKiev/piccolo.txt

chmod 640 /srv/sox/TheGreatGateOfKiev/clarinet.txt

chmod 640 /srv/sox/TheGreatGateOfKiev/horn.txt

chmod 640 /srv/sox/BlueDanube/trunk.txt

chmod 640 /srv/sox/BlueDanube/fiddle.txt

chmod 640 /srv/sox/BlueDanube/viola.txt

chmod 640 /srv/sox/NewWorldSymphony/cello.txt

chmod 640 /srv/sox/NewWorldSymphony/doublebass.txt

chmod 640 /srv/sox/TheJazzSuite/battery.txt

chmod 640 /srv/sox/TheJazzSuite/xylophone.txt

chmod 640 /srv/sox/TheJazzSuite/conductor.txt

ls -l /srv/sox/TheGreatGateOfKiev

ls -l /srv/sox/BlueDanube

ls -l /srv/sox/NewWorldSymphony

ls -l /srv/sox/TheJazzSuite

