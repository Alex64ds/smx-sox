#!/bin/bash

root=$(ls /root)

if [ $root = snap ]; then

    echo "LDAP"

else

    exit 1

fi

if [ -d /srv/nfs ]; then

    echo ""

else

    mkdir /srv/nfs

fi

ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H ldap://192.168.5.103 | grep invertebrados | grep ou | cut -d: -f2 | cut -d" " -f2 | cut -d= -f2 | cut -d, -f1 > ldap

inv=$(ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H ldap://192.168.5.103 | grep invertebrados | grep ^cn | cut -d: -f2 | cut -d" " -f2)

mkdir /srv/nfs/$inv

cat ldap | while read line
do

    mkdir /srv/nfs/$inv/$line

done

rm ldap
