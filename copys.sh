#!/bin/bash

copdir=$(date +"%d_%m_%Y")
root=$(ls /root | wc -l)

if [ $root -eq 0 ]; then

    exit 1

fi


if [ -d $copdir ]; then

    echo ""

else

    mkdir $copdir

fi

if [ -d /etc/freeradius/ ]; then

    echo "El servicio <freeradius> no esta instalado"

else

    cp /etc/freeradius/3.0/mods-enabled/ldap $copdir

    cp -R /etc/freeradius/3.0/sites-enabled/* $copdir

    cp /etc/freeradius/3.0/clients.conf $copdir

fi
cp /etc/hosts $copdir

if [ -d /etc/phpldapadmin/config.php ]; then

    echo "El servicio <phpldapadmin> no esta instalado"

else

    cp /etc/phpldapadmin/config.php $copdir

fi

if [ -d /etc/default/dnsmasq ]; then

    echo "El servicio <dnsmasq> no esta instalado"

else

    cp /etc/default/dnsmasq $copdir

    cp /etc/dnsmasq.conf $copdir

    cp /etc/dnsmasq.d/wifi.conf $copdir

fi
cp /etc/NetworkManager/dispatcher.d/hook-network-manager $copdir

if [ -d /etc/apache2/sites-available/000-default.conf ]; then

    echo "El servicio <apache> no esta instalado"

else

    cp /etc/apache2/sites-available/000-default.conf $copdir

fi

if [ -d /etc/ssh/sshd_config ]; then

    echo "El servicio <ssh> no esta instalado"

else

    cp /etc/ssh/sshd_config $copdir

fi

if [ -d /etc/netplan/00-installer-config.yaml ]; then

    echo "Esto esta preparado para ejecutarse en un servidor"

else

    cp /etc/netplan/00-installer-config.yaml $copdir

fi
if [ -d /etc/hostapd/hostapd.conf ]; then

    echo "El servicio <hostapd> no esta instalado"

else

    cp /etc/hostapd/hostapd.conf $copdir

    cp /etc/default/hostapd $copdir

fi

ls $copdir
