#!/bin/bash

rt=$(sudo ls /root | wc -l )

us=$( whoami )

ipser=$( dig ubuntusrv.smx2023.net | tail -n 7 | head -n 1 | cut -f5 )

grp=$(ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H ldap://ubuntusrv.smx2023.net | grep $us | sed '2!d' | cut -d "," -f2 | cut -d= -f2)

compmount=$(mount | grep "$HOME/shared/smx2023.net/exam-ldap-nfs" | wc -l)

num=$(echo $us | grep -o '[0-9]\+')

nunum=$( echo $nunum | sed 's/0//g')

let odd=nunum%2

if [ $compmount -ne 0 ]; then

    echo "EL DIRECTORIO $HOME/shared/smx2023.net/exam-ldap-nfs YA ESTA MONTADO POR TANTO NO HACE FALTA VOLVER A MONTARLO"

    exit 0

fi


if [ $rt -eq 0 ]; then

    echo "EL USUARIO $us NO TIENE PERMISO DE ROOT POR TANTO NO PODRÁ MONTAR NADA"

    logger "permissions=no"

    exit 0

fi

if [ -d $HOME/shared ]; then

    echo " "

else

    mkdir $HOME/shared

fi

if [ -d $HOME/shared/smx2023.net ]; then

    echo " "

else

    mkdir $HOME/shared/smx2023.net

fi

if [ -d $HOME/shared/smx2023.net/exam-ldap-nfs ]; then

    echo " "

else

    mkdir $HOME/shared/smx2023.net/exam-ldap-nfs

    logger "Clidirects created"

fi

if [ $grp = trasgos ]; then

    logger "trasgo=si"

    sudo mount $ipser:/srv/nfs/exam-ldap-nfs $HOME/shared/smx2023.net/exam-ldap-nfs

    mnt=$(mount | grep "$HOME/shared/smx2023.net/exam-ldap-nfs")

    if [ -n "$mnt" ]; then

        logger "montsi"

        echo "el directorio se montó correctamente"

    else

        logger "montno"

        echo "el directorio no fue montado correctamente"

    fi

    if [ $odd -eq 0 ]; then

        if [ -d $HOME/shared/smx2023.net/exam-ldap-smb ]; then

            echo " "

        else

            mkdir $HOME/shared/smx2023.net/exam-ldap-smb

            logger "Clidirects created"

        fi

        sudo mount $ipser:/srv/samba/exam-ldap-smb $HOME/shared/smx2023.net/exam-ldap-smb

         mnt=$(mount | grep "$HOME/shared/smx2023.net/exam-ldap-smb")

        if [ -n "$mnt" ]; then

            logger "montsi"

            echo "el directorio se montó correctamente"

        else

            logger "montno"

            echo "el directorio no fue montado correctamente"

        fi

    else

        logger "goblin=no"

        echo "EL USUARIO CON EL QUE ESTAS LOGADO NO PERTENECE AL GRUPO DE LOS GOBLINS"

    fi

fi

exit 0
