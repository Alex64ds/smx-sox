#!/bin/bash

#Author: Alex Chocano

root=$(ls /root | wc -l)

if [ $root -eq 0 ]; then

    exit 1

fi

place=$1

day=$( date +%d)

month=$( date +%m)

year=$( date +%Y)

timee=$( date +%T)

if [ $# -eq 0 ]; then

    echo "CORRECT EXECUTION ./sources.sh senia or home"

else

    if [ $place = senia ]; then

        sour=$( cat /etc/apt/sources.list)
        echo "$sour" > /root/sources.list.$year$day$month-$timee
        echo "deb http://tic.ieslasenia.org/ubuntu jammy main universe restricted multiverse" > /etc/apt/sources.list
        echo "deb http://tic.ieslasenia.org/ubuntu jammy-updates main universe restricted multiverse" >> /etc/apt/sources.list
        echo "deb http://tic.ieslasenia.org/ubuntu jammy-security main universe restricted multiverse" >> /etc/apt/sources.list
        echo "deb http://tic.ieslasenia.org/ubuntu jammy-backports main universe restricted multiverse" >> /etc/apt/sources.list
        apt-get remove appstream command-not-found
        sudo dpkg --remove-architecture i386
        apt update
        echo "APT CATALOG AND REPOSITORIES UPDATED"


    elif [ $place = home ]; then

        sour=$( cat /etc/apt/sources.list)
        echo "$sour" > /root/sources.list.$date-$timee
        echo "deb http://es.archive.ubuntu.com/ubuntu jammy main universe restricted multiverse" > /etc/apt/sources.list
        echo "deb http://es.archive.ubuntu.com/ubuntu jammy-updates main universe restricted multiverse" >> /etc/apt/sources.list
        echo "deb http://es.archive.ubuntu.com/ubuntu jammy-security main universe restricted multiverse" >> /etc/apt/sources.list
        echo "deb http://es.archive.ubuntu.com/ubuntu jammy-backports main universe restricted multiverse" >> /etc/apt/sources.list
        apt-get remove appstream command-not-found
        sudo dpkg --remove-architecture i386
        apt update
        echo "APT CATALOG AND REPOSITORIES UPDATED"

    else

        echo "ONLY ACCEPT senia or home"

    fi

fi
